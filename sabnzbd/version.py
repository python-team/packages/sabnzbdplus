# This file will be patched by setup.py
# The __version__ should be set to the branch name
# Leave __baseline__ set to unknown to enable setting commit-hash
# (e.g. "develop" or "1.2.x")

# You MUST use double quotes (so " and not ')
# Do not forget to update the appdata file for every major release!

__version__ = "4.2.3"
__baseline__ = "e7e06dea419a03bfe179af64b7a69398f652a7b9"
